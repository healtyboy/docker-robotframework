FROM ubuntu:18.10

RUN apt-get update \
    && apt-get install -y python-pip \
    && apt-get install -y wget \
    && pip install robotframework \
    && pip install robotframework-requests \
    && pip install robotframework-selenium2library \
    && pip install robotframework-httplibrary \
    && pip install robotframework-excellibrary \
    && pip install robotframework-sshlibrary \
    && pip install robotframework-pabot \
    && pip install xlwt \
    && pip install xlutils \
    && pip install XlsxWriter \
    && pip install xlrd

RUN mkdir  /apps \
    && mkdir  /logs \
    && mkdir  /data

WORKDIR /apps
